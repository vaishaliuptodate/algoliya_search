$(document).ready(function(){
  $('.logo-bar').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 3000,
    centerMode: false,
    swipe: true,
    initialSlide: 0,
    prevArrow: $('.logo-bar-slick-prev'),
    nextArrow: $('.logo-bar-slick-next'),
    responsive: [{
        breakpoint: 768,
        settings: {
          arrows: false,
          prevArrow: false,
          nextArrow: false,
          centerMode: true,
          slidesToShow: 3
        }
    }, {
      breakpoint: 520,
      settings: {
        arrows: false,
        prevArrow: false,
        nextArrow: false,
        centerMode: true,
        slidesToShow: 2
      }
    }]
  });

  const resize = () => {
    if ($('.logo-bar').width() > 768) {
      $('.logo-bar-slick').show();
    } else {
      $('.logo-bar-slick').hide();
    }
  }

  $(window).resize(resize);
  resize();
});
